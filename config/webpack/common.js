const   entry = require('./common/entry'),
       output = require('./common/output');

const plugins = require('./common/plugins'),
      modules = require('./common/module');

const devServer  = require('./common/server');

const path = require('./constants').path;


module.exports = { entry,
                   output,
                   plugins, 
                   module: modules,
                   devServer };
