
const HtmlWebpackPlugin = require('html-webpack-plugin'),
                webpack = require('webpack'); 

const path = require('../constants').path;


let plugins, cfg = {};


cfg.html = { template: path.src + '/index.html',
             filename: 'index.html',
               inject: 'body' };


cfg.chunks = { name: ["app", 
                      "vendor", 
                      "polyfills"] };

plugins = [ new HtmlWebpackPlugin(cfg.html),
            new webpack.optimize.CommonsChunkPlugin(cfg.chunks) ];


module.exports = plugins;
