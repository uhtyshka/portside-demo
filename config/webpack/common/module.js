const cfg = {};


cfg.loaders = [
    { test: /\.js$/,  loaders: 'babel-loader', exclude: /node_modules/ },
    { test: /\.jsx$/, loaders: 'babel-loader', exclude: /node_modules/ } ];


module.exports = cfg;
