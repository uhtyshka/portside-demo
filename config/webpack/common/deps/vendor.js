module.exports = [
    'history', 
    'lodash',
    'prop-types', 
    'react',
    'react-dom',
    'react-redux',
    'react-router',
    'react-router-dom',
    'react-router-redux',
    'redux',
    'redux-devtools-extension',
    'immutable',
    'redux-immutable',
    'babel-polyfill',
    'styled-components',
    'classnames',
    'material-ui',
    'material-ui/styles',
    'redux-saga' ];
