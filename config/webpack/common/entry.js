const path = require('../constants').path;

const deps = { vendor: require('./deps/vendor') };


let cfg = {};

cfg.app = path.src + '/app/app.js';
cfg.vendor = deps.vendor;


module.exports = cfg;
