const path = require('../constants').path;

const devServer = {
    port: 3000,
    historyApiFallback: true
};

module.exports = devServer;
