const path = require('path');


let cfg = { output: {},
              path: {},
               env: {} };

let env = process.env.NODE_ENV || 'development';


cfg.path = {  root: path.resolve(),
             build: path.resolve('build'),
               src: path.resolve('src'),
               cfg: path.resolve('config') };


cfg.output = { client: cfg.path.build + '/client.js',
               vendor: cfg.path.build + '/vendor.js',
                  css: cfg.path.build + '/style.css' };


cfg.env = { prod: env === "production",
             dev: env === "development" };


module.exports = cfg;
