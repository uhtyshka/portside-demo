import    React from 'react';
import ReactDOM from 'react-dom';

import        { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import  createHistory from 'history/createBrowserHistory';
import createAppStore from './store';

import App from "./components";


const MOUNT_NODE = document.getElementById("app"),
         history = createHistory(),
           store = createAppStore(history);

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>
    , MOUNT_NODE);
