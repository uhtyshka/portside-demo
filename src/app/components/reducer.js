import { combineReducers } from 'redux-immutable';
import          { fromJS } from 'immutable';

import    homeReducer from './child/Home/core/reducer';
import detailsReducer from './child/Details/core/reducer';


const rootReducer = combineReducers({
    home: homeReducer,
    details: detailsReducer });
    

export default rootReducer;

