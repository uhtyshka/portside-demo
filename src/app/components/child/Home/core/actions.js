export const USERLIST_REQUEST = "USERLIST_REQUEST";
export const USERLIST_SUCCESS = "USERLIST_SUCCESS";
export const USERLIST_FAILURE = "USERLIST_FAILURE";


export { fetchUsers }

function fetchUsers(){
    return { type: USERLIST_REQUEST }; }
