import { takeLatest, call, put } from 'redux-saga/effects';

import api from '../../../../services/api';

import { USERLIST_REQUEST,
         USERLIST_SUCCESS,
         USERLIST_FAILURE } from './actions';


function* getUserList() {
    try{
        const userlist = yield call(api.fetchUserlist);
        yield put({ type: USERLIST_SUCCESS, data: { userlist} }); }
    catch(error) {
        let message = "fetching data error"; 
        yield put({ type: USERLIST_FAILURE, data: { error: message } }); }
}

function* watchUserlist(){
    yield takeLatest( USERLIST_REQUEST, getUserList ); }

export default watchUserlist;
