import { fromJS } from 'immutable';

import { USERLIST_REQUEST,
         USERLIST_SUCCESS,
         USERLIST_FAILURE } from './actions';

const init = fromJS({ userlist: [],
                      fetching: false,
                         error: null});

function userlistReducer(state=init, action){
    switch(action.type){
        case USERLIST_REQUEST: {
            return state.set("fetching", true); }
        case USERLIST_SUCCESS: {
            return state.set("userlist", action.data.userlist)
                        .set("fetching", false); }
        case USERLIST_FAILURE: {
            return state.set("error", action.data.error)
                        .set("fetching", false); }
        default: return state;
    }
}


export default userlistReducer;

export { isUsersFetchingError,
            getUsersFromStore,
              isUsersFetching };

function getUsersFromStore(state){
    return state.getIn(['app', 'home', 'userlist']); }
function isUsersFetching(state){
    return state.getIn(['app', 'home', 'fetching']); }
function isUsersFetchingError(state){
    return state.getIn(['app', 'home', 'error']); }
