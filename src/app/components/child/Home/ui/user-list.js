import React from 'react';

export default function UserList(props){

    return props.list.map(mapUserData);

    function mapUserData(user, idx){
        return ( 
            <div className="user-box" dataid={user.id} key={idx}>
                <div className="user-name" dataid={user.id}> 
                    <h4 dataid={user.id}>{user.username} </h4>
                </div>
                <div className="user-email" dataid={user.id}>
                    <h6 dataid={user.id}>{user.email} </h6>
                </div>
                <hr/>
            </div> );
    } 
}
