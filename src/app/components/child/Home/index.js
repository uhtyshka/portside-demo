import React from 'react';

import { connect } from 'react-redux';
import    { push } from 'react-router-redux';

import { fetchUsers } from './core/actions';

import { isUsersFetchingError,
            getUsersFromStore,
              isUsersFetching } from './core/reducer';

import UserList from './ui/user-list';


class Home extends React.Component{
    
    constructor(props){
        super(props);

        this.state = { users: [] }; 
        this.onUserClick = this.onUserClick.bind(this);
    }


    onUserClick(e){ 
        if(!e.target.attributes || !e.target.attributes.dataid){
            return false;}

        const userId = e.target.attributes.dataid.value; 
        this.props.goToDetails(userId);
    }


    componentDidMount(){
        this.props.fetchUsers(); }
    

    render(){
        if(this.props.isError){
            return ( <div className="error-box"> {this.props.isError} </div> ); } 
        if(this.props.isFetching){
            return ( <div className="async-placeholder"> Loading... </div> ); }

        return(
            <div className="list-wrapper" onClick={this.onUserClick}>
                <UserList list={this.props.users}>
                </UserList>
            </div>);
    } 
}


const mapStateToProps = (state) => ({
       isError: isUsersFetchingError(state),
    isFetching: isUsersFetching(state),
         users: getUsersFromStore(state) });

const mapDispatchToProps = (dispatch) => ({
    goToDetails: (id) => { dispatch(push(`/details/${id}`)) },
     fetchUsers: ()=> { dispatch(fetchUsers()) } });


export default connect(mapStateToProps, mapDispatchToProps)(Home);
