import React from 'react';

import { connect } from 'react-redux';

import { fetchUserData } from './core/actions';

import { getFetchedUserFromStore,
             isUserFetchingError, 
                  isUserFetching } from './core/reducer';



class Details extends React.Component{
    
    constructor(props){
        super(props); 
        this.state = { user: {} }; 
    }


    componentDidMount(){
        this.props.getUserData(this.props.match.params.id); }

    render(){
        if(this.props.isError ){
            return ( <div className="error-box"> {this.props.isError} </div> ); } 
        if(this.props.isFetching || !this.props.user.id){
            return ( <div className="async-placeholder"> Loading... </div> ); }

        return(
            <div className="user-details-wrapper">
                <div className="user-name">
                    {this.props.user.name}
                </div>
                <div className="user-login">
                    {this.props.user.login}
                </div>
                <div className="user-email">
                    {this.props.user.email}
                </div>
                <div className="user-phone">
                    {this.props.user.phone}
                </div>
                <div className="user-website">
                    {this.props.user.website}
                    <hr/>
                </div>
                <div className="user-address">
                    <div className="user-city">
                        {this.props.user.address.city}
                    </div>
                    <div className="user-street">
                        {this.props.user.address.street}
                    </div>
                    <div className="user-suite">
                        {this.props.user.address.suite}
                    </div>
                    <div className="user-zipcode">
                        {this.props.user.address.zipcode}
                    </div>
                    <div className="user-city">
                        {this.props.user.address.city}
                    </div>
                </div>
                <div className="user-company"> 
                    <div className="user-company-name">
                        {this.props.user.company.name}
                    </div>
                    <div className="user-company-catchphrase">
                        "{this.props.user.company.catchPhrase}"
                    </div>
                </div>
            </div>
        );
    } 
}

const mapStateToProps = (state) => ({ 
       isError: isUserFetchingError(state),
    isFetching: isUserFetching(state),
          user: getFetchedUserFromStore(state) }); 

const mapDispatchToProps = (dispatch) => ({
    getUserData: (id)=> { dispatch(fetchUserData(id)) } });


export default connect(mapStateToProps, mapDispatchToProps)(Details);
