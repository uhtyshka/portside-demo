import { takeLatest, call, put } from 'redux-saga/effects';

import api from '../../../../services/api';

import { USER_REQUEST,
         USER_SUCCESS,
         USER_FAILURE } from './actions';


function* getUserDetails(user) {
    try{
        const userData = yield call(api.fetchUserData, user.data.id);
        yield put({ type: USER_SUCCESS, data: { user: userData } }); }
    catch(error) {
        let message = "some error"; 
        yield put({ type: USER_FAILURE, data: { error: message } }); }
}


function* watchUserDetails(){
    yield takeLatest( USER_REQUEST, getUserDetails ); }

export default watchUserDetails;
