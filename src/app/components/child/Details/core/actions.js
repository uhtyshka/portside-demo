export const USER_REQUEST = "USER_REQUEST";
export const USER_SUCCESS = "USER_SUCCESS";
export const USER_FAILURE = "USER_FAILURE";


export { fetchUserData };

function fetchUserData(id){
    return { type: USER_REQUEST, data: { id } }; }
