import { fromJS } from 'immutable';

import { USER_REQUEST,
         USER_SUCCESS,
         USER_FAILURE } from './actions';

const init = fromJS({ fetching: false,
                          user: [], 
                         error: null});

function detailsReducer(state=init, action){
    switch(action.type){
        case USER_REQUEST: {
            return state.set("fetching", true); }
        case USER_SUCCESS: {
            return state.set("user", action.data.user)
                        .set("fetching", false); }
        case USER_FAILURE: {
            return state.set("error", action.data.error)
                        .set("fetching", false); }
        default: return state;
    }
}


export default detailsReducer;

export { getFetchedUserFromStore, 
             isUserFetchingError,
                  isUserFetching };


function getFetchedUserFromStore(state){
    return state.getIn(['app', 'details', 'user']); }
function isUserFetching(state){
    return state.getIn(['app', 'details', 'fetching']); }
function isUserFetchingError(state){
    return state.getIn(['app', 'details', 'error']); }
