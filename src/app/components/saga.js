import { all, call } from 'redux-saga/effects';

import    watchUserlist from './child/Home/core/saga';
import watchUserDetails from './child/Details/core/saga';


export default function* rootSaga(){ 
     yield all([ call(watchUserlist),
                 call(watchUserDetails) ]);
}

