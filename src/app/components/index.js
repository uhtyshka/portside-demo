import React from 'react';

import { Switch, 
          Route } from 'react-router-dom';

import    Home from './child/Home';
import Details from './child/Details';


export default function App(){
    return (
        <div className="app-wrapper">
            <Switch>
                <Route exact path="/" component={Home}/ >
                <Route path="/details/:id" component={Details}/>
            </Switch>
        </div>
    );
}
