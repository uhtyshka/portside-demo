import  { applyMiddleware,
              createStore,
                  compose } from 'redux';

import { routerMiddleware,
          LOCATION_CHANGE } from 'react-router-redux';

import  { combineReducers } from 'redux-immutable';
import           { fromJS } from 'immutable';

import createSagaMiddleware from 'redux-saga';

import rootReducer from './components/reducer';
import    rootSaga from './components/saga';


const sagaMiddleware = createSagaMiddleware(),
        appInitState = fromJS({}),
     routerInitState = fromJS({ location: null });


export default createAppStore;


function createAppStore(history){
    
    const middlewares = [ sagaMiddleware, routerMiddleware(history) ],
            enhancers = [ applyMiddleware(...middlewares) ],
             reducers = combineReducers({ route: routerReducer, app: rootReducer });

    const store = createStore(reducers, appInitState, composeEnhancer()(...enhancers));

    function composeEnhancer() {
        return window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ shouldHotReload: false });
    } 

    sagaMiddleware.run(rootSaga);

    return store;

    function routerReducer(state=routerInitState, action){
        switch(action.type){
            case LOCATION_CHANGE: {
                return state.merge({ location: action.payload }); }
            default: return state;
        }
    } 
}
