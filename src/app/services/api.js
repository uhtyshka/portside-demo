import request from './api/request';

const BASE = 'https://jsonplaceholder.typicode.com/';

class api {
    fetchUserlist(){
        const url = `${BASE}users/`;
        return request.getData(url); }

    fetchUserData(id){
        const url = `${BASE}users/${id}`;
        return request.getData(url); }
}


export default new api();
