class request {
    getData(url){
        return new Promise((resolve,reject) => {
            return fetch(url)
                    .then((resp)=>{
                        return resp.status !== 200 ? reject(resp) : resp; })
                    .then((resp)=> {
                        return resp.json(); })
                    .then((resp)=>{
                        return resolve(resp); })
                    .catch((error)=>{
                        return reject(error); });
        });
    }
}


export default new request();
